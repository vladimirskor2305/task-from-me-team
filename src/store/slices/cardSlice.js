import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import JsonPlaceholderService from "../../services/JsonPlaceholder";

const initialState = {
    cardInfo: [],
    cardLoadingStatus: 'idle',
    selectedCard: {},
    likedLenght: 0,
    //filters
    likedCardInfo: [],
    term: '', 
}


export const fetchCardsInfo = createAsyncThunk(
    'cardsInfo/fetchCardsInfo',
    async () => {
        const {getAllPhotos} = JsonPlaceholderService()
        return await getAllPhotos()
    }
);

export const getCardInfo = createAsyncThunk(
    'cardsInfo/getCardInfo',
    async (id) => {
        const {getCardInfo} = JsonPlaceholderService()
        return await getCardInfo(id)
    }
);



const cardInfoSlice = createSlice({
    name: 'cardInfo',
    initialState,
    reducers: {
        cardDeleted: (state, action) => {
            state.cardInfo = state.cardInfo.filter(item => item.id !== action.payload)
        },
        onCardSelected : (state, action) => {
            const selectItem =   state.selectedCard = state.cardInfo.filter(item => item.id === action.payload)
           state.selectedCard = selectItem
        },
        toggleLiked: (state, action) => {

            const toggleCard = state.cardInfo.find(item => item.id === action.payload)
            toggleCard.liked = !toggleCard.liked
            const noRepeatCards = state.likedCardInfo.find(item => item.id === action.payload)

            if(toggleCard.liked && !noRepeatCards)  {
                state.likedCardInfo.push(toggleCard)
                toggleCard.liked = true
            } else {
                state.likedCardInfo = state.likedCardInfo.filter(item => item.liked && item.id !== action.payload )
            }
 
            state.likedLenght = state.likedCardInfo.length
        },
        searchCard: (state, action) => {
            state.term = action.payload
        },
    },
    extraReducers: (builder) => {
        builder
            .addCase(fetchCardsInfo.pending, state => {state.cardLoadingStatus = 'loading'})
            .addCase(fetchCardsInfo.fulfilled, (state, action) => {
                state.cardLoadingStatus = 'idle';
                
                state.cardInfo = action.payload;
            })
            .addCase(fetchCardsInfo.rejected, state => {
                state.cardLoadingStatus = 'error';
            })

            .addCase(getCardInfo.pending, state => {state.cardLoadingStatus = 'loading'})
            .addCase(getCardInfo.fulfilled, (state, action) => {
                state.selectedCard = action.payload
                state.cardLoadingStatus = 'idle';
            })
            .addCase(getCardInfo.rejected, state => {
                state.cardLoadingStatus = 'error';
            })
            .addDefaultCase(() => {})
    }
});

const {actions, reducer} = cardInfoSlice;

export default reducer;
export const {
    onCardSelected,
    toggleLiked,
    likedList,
    searchCard,
} = actions;
import { useSelector, useDispatch } from "react-redux";
import { useNavigate, useParams } from 'react-router-dom';
import { useEffect, useState } from "react";
import Modal from "../modal/Modal";

import { getCardInfo } from "../../store/slices/cardSlice";
import Spinner from "../spinner/Spinner";

const FullInfoPage = () => {


    const {cardID} = useParams()
    const [modal, setModal] = useState(false)

    useEffect(() => {
        dispatch(getCardInfo(cardID))
    },[cardID])

    const dispatch = useDispatch()
    const navigate = useNavigate()

    const {selectedCard, cardLoadingStatus} = useSelector(state => state.cardInfoSlice)

    if (cardLoadingStatus === "loading") {
        return <Spinner/>;
    } else if (cardLoadingStatus === "error") {
        return <h5 className="text-center mt-5">Ошибка загрузки</h5>
    }


    const {id, albumId, title, url, thumbnailUrl} = selectedCard





    
    return (

        <div style={{padding: '30px 230px'}}>
           
            {selectedCard && (
                <li 
                     key={id}
                     className="comics__item"
                     >
                     <img src={thumbnailUrl} alt="ultimate war" className="comics__item-img"/>
                     <div className='comics__item-actions' >
                         <div>
                            <h4>Card ID:{id}</h4>
                            <h4>Album ID:{albumId}</h4>
                            <h4>Card title:{title}</h4>
            
                            <button onClick={() => setModal(true)} >url: {url}</button>
                            <Modal active={modal} toggleActive={setModal}>
                                <img src={url} alt="image"/>
                            </Modal>
                                

                        

                         </div>

                         

                         <button
                             onClick={() => navigate(-1)}
                             >
                                 <h3>{`<== back`}</h3>
                         </button>
                     </div>
                </li>     
            )}
        </div>

    )
}

export default FullInfoPage;
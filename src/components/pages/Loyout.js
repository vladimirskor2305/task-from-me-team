import { Outlet, NavLink } from "react-router-dom"
import { useSelector } from 'react-redux';

import './loayout.scss'
import SearchPanel from "../searchPanel/SearchPanel";


const Layout = () => {
    
    const {likedLenght} = useSelector(state => state.cardInfoSlice)
    
    return (
        <>
            <header className="app__header">
                <h1 className="app__title">
                    Task from Me-Team
                </h1>

                <SearchPanel/>

                
                <nav className="app__menu">
                    <ul>
                        <li><NavLink  style={({isActive}) => ({color: isActive ? 'red' : null})} to='/'>Main page</NavLink></li>
                        <li><NavLink  style={({isActive}) => ({color: isActive ? 'red' : null})} to='favorites'>Favorites:{likedLenght}</NavLink></li>
                    </ul>
                </nav>
            </header>

            <div>
                <Outlet/>
            </div>

        </>
    )

}
export default Layout
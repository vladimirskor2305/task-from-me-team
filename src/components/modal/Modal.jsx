import './modal.scss'

const Modal = ({active, toggleActive, children}) => {

    return (
        <div 
            onClick={() => toggleActive(false)} 
            className={active ? 'modal active' : 'modal' } >
            <div onClick={e => e.stopPropagation()}  className="modal__content" >
                {children}
            </div>
        </div>
    )
}

export default Modal
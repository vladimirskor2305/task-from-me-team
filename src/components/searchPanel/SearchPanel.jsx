

import { useDispatch, useSelector } from 'react-redux';
import { useEffect } from 'react';

import './searchPanel.scss';



import { searchCard} from '../../store/slices/cardSlice'



const SearchPanel = () => {

   
    const {term} = useSelector(state => state.cardInfoSlice)


    const dispatch = useDispatch()

    const onUpdateSearch = (e) => {
        dispatch(searchCard(e.target.value))
    }

    return (
        <>
            <input 
                
                type="text"
                className="searchField"
                placeholder="search card by title..."
                value={term}
                onChange={(e) => onUpdateSearch(e)}/>
        </>
    )
}

export default SearchPanel
import { Link } from 'react-router-dom';
import {useEffect} from 'react';
import { useDispatch, useSelector } from 'react-redux';


import Spinner from '../spinner/Spinner'
import './cardList.scss';


import { fetchCardsInfo, toggleLiked} from '../../store/slices/cardSlice';


const CardList = () => {

    const {cardInfo, term,  cardLoadingStatus} = useSelector(state => state.cardInfoSlice)
    const dispatch = useDispatch()

    useEffect(() => {
        if(!localStorage.getItem("persist:root")) {
            dispatch(fetchCardsInfo())
        }
    }, []);


    if (cardLoadingStatus === "loading") {
        return <Spinner/>;
    } else if (cardLoadingStatus === "error") {
        return <h5 className="text-center mt-5">Ошибка загрузки</h5>
    }

    const searchCard = (data, EnterText) => {
		if(EnterText.length === 0) {
			return data
		}

		return data.filter(items => {
            return items.title.indexOf(EnterText) > -1
        })

	}

    const visibleCardInfo = searchCard(cardInfo, term)


    return (

        <div>
            <ul className="comics__grid">
                {cardInfo && visibleCardInfo.map(item => (
                    <li
                        key={item.id}
                        className="comics__item">

                        <img src={item.thumbnailUrl} alt="ultimate war" className="comics__item-img"/>

                        <div>
                            <div className="comics__item-name">{item.title}</div>
                            <div className="comics__item-number">album number:{item.albumId}</div>
                        </div>

                        <div className='comics__item-actions' >
                            <button className='btnMore' >
                                <Link to={`/${item.id}`}>More</Link>
                            </button>

                            <button
                                    onClick={() => {
                                        dispatch(toggleLiked(item.id))  
                                    }} 
                                    className='btn-svg' >

                                        {item.liked ? (

                                        <svg 
                                            id="Layer_1" data-name="Layer 1" 
                                            xmlns="http://www.w3.org/2000/svg" 
                                            style={{fill: 'red', fillRule: 'evenodd'}}
                                            viewBox="0 0 122.88 107.39">
                                            <path className="cls-1" d="M60.83,17.18c8-8.35,13.62-15.57,26-17C110-2.46,131.27,21.26,119.57,44.61c-3.33,6.65-10.11,14.56-17.61,22.32-8.23,8.52-17.34,16.87-23.72,23.2l-17.4,17.26L46.46,93.55C29.16,76.89,1,55.92,0,29.94-.63,11.74,13.73.08,30.25.29c14.76.2,21,7.54,30.58,16.89Z"/>
                                        </svg>

                                    ) : (

                                        <svg
                                            xmlns="http://www.w3.org/2000/svg"
                                            x="0"
                                            y="0"
                                            enableBackground="new 0 0 122.88 107.68"
                                            version="1.1"
                                            viewBox="0 0 122.88 107.68"
                                            xmlSpace="preserve"
                                            >
                                            <path d="M61.43 13.53C66.76 7.51 72.8 3.69 78.96 1.69c6.48-2.1 13.07-2.15 19.09-.6 6.05 1.55 11.52 4.72 15.74 9.03 5.58 5.7 9.09 13.36 9.09 22.02 0 13.7-6.6 26.75-17.42 39.37-10.14 11.83-24.05 23.35-39.61 34.73a7.458 7.458 0 01-8.5.22v.01l-.03-.02v.01l-.02-.01-.21-.15c-4.46-2.92-8.75-5.91-12.8-8.94-4.05-3.03-8.01-6.22-11.83-9.56C12.58 70.42 0 51.4 0 32.13c0-8.8 3.44-16.44 8.93-22.08 4.25-4.37 9.73-7.51 15.79-9.03 5.99-1.5 12.57-1.4 19.05.69 6.22 2 12.32 5.83 17.66 11.82zm22.08 2.34c-5.49 1.78-11 6.15-15.51 13.91a7.355 7.355 0 01-2.85 2.93A7.429 7.429 0 0155 29.97c-4.5-7.82-10.14-12.27-15.78-14.08-3.71-1.19-7.46-1.25-10.88-.4h-.02c-3.35.83-6.37 2.56-8.7 4.95-2.87 2.95-4.67 7-4.67 11.7 0 14.53 10.59 29.82 27.3 44.43 3.28 2.87 6.95 5.82 10.95 8.81 2.61 1.96 5.35 3.92 8.04 5.74 13.03-9.76 24.53-19.53 32.9-29.3 8.58-10 13.8-19.92 13.8-29.68 0-4.55-1.84-8.58-4.76-11.57-2.38-2.42-5.43-4.2-8.8-5.06-3.4-.88-7.15-.84-10.87.36z"></path>
                                        </svg>
                                    )}
                            </button>
                        </div>
                    </li>
                ))}
            </ul>
        </div>
    )
}


export default CardList;
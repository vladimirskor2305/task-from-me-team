import { Routes, Route,  } from "react-router-dom";

import Layout from "../pages/Loyout";
import MainPage from "../pages/MainPage";
import FullInfoPage from "../pages/FullInfoPage";
import FavoritesPage from "../pages/Favorites";


function App() {
  return (
    <div className="app">
       <Routes>
          <Route path="/" element={<Layout/>} >
            <Route index element={<MainPage/>}/>
            <Route path="/:cardID" element={<FullInfoPage/>}/>
            <Route path="favorites" element={<FavoritesPage/>}/>
            {/* <Route path="favorites/:cardID" element={<FullInfoPage/>}/> */}
          </Route> 
        </Routes>
    </div>
  );
}

export default App;

import { useHttp } from "../hooks/useHttp"

const JsonPlaceholderService = () => {

    const {request} = useHttp()

    const _apiBase = 'https://jsonplaceholder.typicode.com/photos'

    const getAllPhotos = async () => {

        const _addNewField = (card) => {

            return card.map(filds => (
                {
                    ...filds,
                    liked: false,
                }
            ))
    
        }

        const res = await request(`${_apiBase}?_limit=300`);
        const newData =  _addNewField(res)
        const alfOrder = newData.slice().sort((a, b) => a.title[0] > b.title[0] ? 1 : -1 )
        const sortAlbumNumber = alfOrder.slice().sort((a, b) => a.albumId - b.albumId)
        return  sortAlbumNumber
        
    }

    const getCardInfo = async (id) => {
        const res = await request(`${_apiBase}/${id}`)
        return res
    }
    
    
    return {getAllPhotos, getCardInfo}
}

export default JsonPlaceholderService
